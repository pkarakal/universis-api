import Rule from "./../models/rule-model";

/**
 * @class StudentAttributeRule
 * @constructor
 * @augments Rule
 */
export default class StudentAvailableClassRule extends Rule {
    constructor(obj) {
        super('Rule', obj);
    }

    /**
     * Validates an expression against the current student
     * @param {*} obj
     * @param {Function} done
     */
    validate(obj, done) {
        try {
            done(null, this.success('SUCC', 'Rule passed'));
        }
        catch(e) {
            done(e);
        }
    }
}