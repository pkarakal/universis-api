import passport from 'passport';
import BearerStrategy from 'passport-http-bearer';
import {HttpForbiddenError, HttpBadRequestError} from '@themost/common/errors';
import {OAuth2ClientService} from "./services/oauth2-client-service";

/***
 * Implements passport HTTP bearer authorization
 */
function useHttpBearerAuthorization() {
    // passport bearer authorization strategy
    // https://github.com/jaredhanson/passport-http-bearer#usage
    passport.use(new BearerStrategy({
        passReqToCallback: true
        },
        /**
         * @param {Request} req
         * @param {string} token
         * @param {Function} done
         */
        function(req, token, done) {
            /**
             * Gets OAuth2 client services
             * @type {*}
             */
            let client = req.context.getApplication().getStrategy(OAuth2ClientService);
            // if client cannot be found
            if (typeof client === 'undefined') {
                // throw configuration error
                return done(new Error('Invalid application configuration. OAuth2 client service cannot be found.'))
            }
            // get token info
            client.getTokenInfo(req.context, token).then(info => {
                if (typeof info === 'undefined' || info === null) {
                    // the specified token cannot be found
                    return done(new HttpBadRequestError('Authentication token cannot be found.'));
                }
                // if the given token is not active throw token expired error
                if (!info.active) {
                    return done(new HttpBadRequestError('Authentication token was expired.'));
                }
                // find user from token info
                return req.context.model('User').where('name').equal(info.username).silent().getItem().then( user => {
                    // user cannot be found and of course cannot be authenticated (throw forbidden error)
                    if (typeof user === 'undefined' || user === null) {
                        return done(new HttpForbiddenError());
                    }
                    // check if user has enabled attribute
                    if (user.hasOwnProperty('enabled') && !user.enabled) {
                        //if user.enabled is off throw forbidden error
                        return done(new HttpForbiddenError('Access is denied. User account is disabled.'));
                    }
                    // otherwise return user data
                    return done(null, {
                        "name": user.name,
                        "authenticationProviderKey": user.id,
                        "authenticationType":'Bearer',
                        "authenticationToken": token,
                        "authenticationScope": info.scope
                    });
                });
            }).catch(err => {
                return done(err);
            });
        }
    ));
}

module.exports.useHttpBearerAuthorization = useHttpBearerAuthorization;

