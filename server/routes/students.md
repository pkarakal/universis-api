### Students Services Summary

The following document contains information about the core services for getting and editing data related to a student.
 
 #### Send message to a student
 
 Sending a message to a student related or not with a student request action
 
     POST /api/Students/{id}/messages/send
     Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
     Content-Type: multipart/form-data
     Accept-Language: el
     
 where authorization header contains user access token received from OAuth2 authentication server,
 content-type defines a multipart/form-data request and accept-language header is the preferred locale 
 for this request.
 
 Form data can contain a field called "file" which is the file (*.csv or *.xlsx) that is going to be uploaded.
       
       POST /api/Students/9999/messages/send HTTP/1.1
       Host: localhost:5001
       Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
       Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
       cache-control: no-cache
       Postman-Token: 34a0b0cc-dd53-425c-9a86-a7dbefed0161
       
       Content-Disposition: form-data; name="attachment"; filename="C:\Projects\universis\universis-teachers\src\assets\img\universis_logo_128_color.png
       
       
       
       Content-Disposition: form-data; name="body"
       
       Welcome
       
       Content-Disposition: form-data; name="action"
       
       31
       ------WebKitFormBoundary7MA4YWxkTrZu0gW--
 
 The response of this operation is a StudentMessage object:
 
     {
         "body": "Welcome",
         "action": 31,
         "student": 9999,
         "recipient": 5678,
         "sender": 1234,
         "owner": 1234,
         "identifier": "9282FB86-EEF0-4271-AC42-846D7DFC9D6C",
         "dateCreated": "2019-04-18T08:26:58.484Z",
         "dateModified": "2019-04-18T08:26:58.530Z",
         "createdBy": 1234,
         "modifiedBy": 1234,
         "id": 23,
         "attachments": [
             {
                 "contentType": "image/png",
                 "owner": 5678,
                 "version": 1,
                 "published": false,
                 "alternateName": "oBxRQQbadN7R",
                 "url": "\\api\\content\\private\\oBxRQQbadN7R",
                 "additionalType": "Attachment",
                 "dateCreated": "2019-04-18T08:26:58.838Z",
                 "dateModified": "2019-04-18T08:26:58.884Z",
                 "createdBy": 1234,
                 "modifiedBy": 1234,
                 "id": 1073
             }
         ]
     }
 
 