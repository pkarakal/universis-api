import express from 'express';
import {TraceUtils,HttpServerError,HttpNotFoundError} from '@themost/common';
import multer from "multer";
let router = express.Router();
// get user storage
const upload = multer({ dest: 'content/user/' });


router.post('/:id/attachments/add', upload.single('file'),async function addAttachment (req, res, next) {
    try {
        // get studentMessage
        let studentMessage = await req.context.model('StudentMessage').where('id').equal(req.params.id).select('id').getTypedItem();
        if (typeof studentMessage === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError('Student message cannot be found'));
        }
        studentMessage.addAttachment(req.file).then(result=>{
            res.json(result);
        }).catch(err=>{
            return next(err);
        });

    } catch (err) {
        return next(err);
    }
});


router.post('/:id/attachments/:attachment/remove',async function removeAttachment (req, res, next) {
    try {
        // get studentMessage
        let studentMessage = await req.context.model('StudentMessage').where('id').equal(req.params.id).select('id').getTypedItem();
        if (typeof studentMessage === 'undefined') {
            // and throw error
            return next(new HttpNotFoundError('Student message cannot be found'));
        }
        studentMessage.removeAttachment(req.params.attachment).then(result=>{
            res.json(result);
        }).catch(err=>{
            return next(err);
        });

    } catch (err) {
        return next(err);
    }
});

module.exports = router;
