import {TraceUtils} from '@themost/common/utils';
import _ from 'lodash';
import {LangUtils} from '@themost/common/utils';
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    try {
        const context = event.model.context;
        /**
         * @type {{courseClass:string|*}}
         */
        const target = event.target;
        const studentCourses = context.model('StudentCourse');

        updateExamTotalStudents(context, target, event.state, function (err) {
            if (err) {
               return callback(err);
            }
            if (event.state === 2) {
                if (target.courseExam) {
                    /**
                     * @type {DataObject|CourseClassModel|*}
                     */
                    const courseExam = courseExam.convert(target['courseClass']);
                    studentCourses.where('course').equal(target['course']).and('student').equal(target['student']).count(function (err, count) {
                        if (err) {
                            callback(err);
                        }
                        else if (count === 1) {
                            callback();
                        }
                        else {
                            //add student course
                            const item = {course: target['course'],
                                student: target['student'],
                                courseTitle: target.name,
                                semester: target.semester,
                                specialty: target.specialty.specialty,
                                units: target.units,
                                coefficient: target.coefficient,
                                courseType: target.courseType,
                                programGroup: target.programGroup,
                                parentCourse: target.parentCourse,
                                hours: target.hours,
                                ects:target.ects
                            };
                            context.unattended(function (cb) {
                                studentCourses.save(item, function (err) {
                                    cb(err);
                                });
                            }, function (err) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return callback(err);
                                }
                                callback();
                            });
                        }
                    });
                }
            }
            else {
                callback();
            }
        });
    }
    catch (e) {
        callback(e)
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
    try {
        const context = event.model.context, target = event.target;
        updateExamTotalStudents(context, target, 4, function (err) {
            if (err) {
                TraceUtils.error(err);
                return callback();
            }
            else {
                callback();
            }
        });
    }
    catch (e) {
        callback(e)
    }
}


function updateExamTotalStudents(context,target, state,callback)
{
    try {
        if (!target.courseExam)
            return callback(null);
        context.model('CourseExam').where('id').equal(target.courseExam).select(['id', 'numberOfGradedStudents']).first(function (err, result) {
            if (err) {
                return callback(err);
            }
            if (_.isNil(result))
                return callback(null);
            let numberOfGradedStudents = LangUtils.parseInt(result["numberOfGradedStudents"]);

            if (state === 1 || state === 2)
                numberOfGradedStudents += 1;
            else
                numberOfGradedStudents -= 1;
            if (numberOfGradedStudents < 0) {
                numberOfGradedStudents = 0;
            }
            context.unattended(function (cb) {
                context.model('CourseExam').save({id: target.courseExam, numberOfGradedStudents: numberOfGradedStudents}, function (err) {
                    cb(err);
                });
            }, function (err) {
                if (err) {
                    TraceUtils.error(err);
                }
                callback();
            });
        });
    }
    catch(e)
    {
        callback(e);
    }
}