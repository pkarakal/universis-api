import util from 'util';
import _ from 'lodash';
import { QueryEntity } from '@themost/query/query';
import {EdmMapping,EdmType} from '@themost/data/odata';
import {HttpServerError, HttpNotFoundError, HttpError,
    HttpForbiddenError, HttpBadRequestError, HttpConflictError} from '@themost/common/errors';
import {TraceUtils} from '@themost/common/utils';
import {DataObject} from "@themost/data/data-object";
import StudentRegisterActionModel from "./student-register-action-model"
import ActionStatusType from "../models/action-status-type-model";
import {ValidationResult} from "../errors";
import moment from 'moment';
import { DataError } from '@themost/common';

@EdmMapping.entityType("Student")
/**
 * @class
 */
class Student extends DataObject {
    constructor() {
        super();
        const self = this;
        self.selector('active',function(callback) {
            try {
                self.attrOf('studentStatus', function(err, result) {
                    if (err) { return callback(err); }
                    const status = self.context.model('StudentStatus').convert(result).getId();
                    callback(null, (status===1));
                });

            }
            catch(er) {
                callback(er);
            }
        });
    }

    programOf(callback) {
        this.attrOf('studyProgram', callback);
    }

    departmentOf(callback) {
        this.attrOf('department', callback);
    }

    inferSemester(year, period, callback) {
        const self=this, context=self.context;
        const query = `select dbo.ufnCalculateStudentSemester (${year},${period},${self.getId()}) as semester`;
        context.db.execute(query, null, function (err, result) {
            if (err) {
                TraceUtils.error(err);
                callback(err);
            }
            callback(null,result[0]["semester"]);
        });
    }

    /**
     * Returns a data queryable for the current student
     * @param {ExpressDataContext} context
     * @returns {DataQueryable}
     */
    @EdmMapping.func("Me", "Student")
    static getMe(context) {
        return context.model('Student').where('user/name').notEqual(null)
            .and('user/name').equal(context.user.name)
            .prepare();
    }

    /**
     *
     * @param {ExpressDataContext} context
     * @return {DataQueryable}
     */
    @EdmMapping.func("Active",EdmType.CollectionOf("Student"))
    static getActiveStudents(context) {
        return context.model('Student').where('studentStatus').equal(1).prepare();
    }


    /**
     *
     */
    @EdmMapping.func("LastPeriodRegistration","StudentPeriodRegistration")
    getLastPeriodRegistration() {
        return this.context.model('StudentPeriodRegistration').where("student").equal(this.getId()).orderByDescending("registrationYear").thenByDescending("registrationPeriod").prepare();
    }

    /**
     *
     */
    @EdmMapping.func("Registrations",EdmType.CollectionOf("StudentPeriodRegistration"))
    getRegistrations() {
        return this.context.model('StudentPeriodRegistration').where("student").equal(this.getId()).prepare();
    }

    /**
     *
     */
    @EdmMapping.func("Courses",EdmType.CollectionOf("StudentCourse"))
    getCourses() {
        return this.context.model('StudentCourse').where("student").equal(this.getId()).prepare();
    }

    /**
     * @returns Promise<DataQueryable>
     */
    @EdmMapping.func("Requests",EdmType.CollectionOf("RequestAction"))
    getRequests() {
        return new Promise((resolve, reject) => {
            return this.context.model('RequestAction').filter({
                $filter: "owner eq me()"
            }, (err, query)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(query);
            });
        });
    }

    /**
     *
     */
    @EdmMapping.func("Grades",EdmType.CollectionOf("StudentGrade"))
    getGrades() {
        const self = this;
        const context=self.context;
        // get student department organization configuration
        return self.getModel().select('id', 'department').expand({
                'name':'department',
                'options':
                    {'$expand':'organization($expand=instituteConfiguration)'

                    }
            }).where('id').equal(self.id).getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            // get showPendingGrades setting
            const showPendingGrades =
                result.department &&
                result.department.organization &&
                result.department.organization.instituteConfiguration &&
                result.department.organization.instituteConfiguration.showPendingGrades;
            if (showPendingGrades) {
                return context.model('StudentGrade').where("student").equal(self.getId()).prepare();
            } else {
                return context.model('StudentGrade').where("student").equal(self.getId()).and('status/alternateName').notEqual('pending').prepare();
            }
        });
    }
    /**
     *
     */
    @EdmMapping.func("Classes",EdmType.CollectionOf("StudentCourseClass"))
    getClasses() {
        return this.context.model('StudentCourseClass').where("student").equal(this.getId()).prepare();
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func("currentRegistration","StudentPeriodRegistration")
    async getCurrentRegistration() {
        const department = await this.context.model('Student')
            .where('id').equal(this.id)
            .select('department/currentYear as currentYear', 'department/currentPeriod as currentPeriod')
            .getItem();
        return this.context.model('StudentPeriodRegistration')
            .where("student").equal(this.id)
            .and("registrationYear").equal(department.currentYear)
            .and("registrationPeriod").equal(department.currentPeriod)
            .prepare();
    }

    /**
     *
     */
    @EdmMapping.param("data", "StudentPeriodRegistration", false, true)
    @EdmMapping.action("currentRegistration","StudentPeriodRegistration")
    commitCurrentRegistration(data) {
        let self = this;
        // get context
        let context = self.context;
        // convert registration to data object
        let registration = context.model("StudentPeriodRegistration").convert(data);
        return new Promise((resolve, reject) => {
            registration.is(':current').then(function (result) {
                if (result) {
                    //execute in unattended mode
                    context.unattended(function (cb) {
                            //do save registration
                            registration.save(context, function (err) {
                                cb(err);
                            });
                        }, function (err) {
                            if (err) {
                                TraceUtils.error(err);
                                return reject(new HttpServerError());
                            }
                            return resolve(registration);
                        });
                }
                else {
                    return reject(new HttpForbiddenError('Invalid registration data. The specified registration is not referred to current academic year and period.'));
                }
            }, function (err) {
                TraceUtils.error(err);
                return reject(new HttpServerError());
            });
        });

    }

    /**
     * @returns {Promise<*>}
     */
    @EdmMapping.func('currentRegistrationStatus', 'PeriodRegistrationEffectiveStatus')
    async getCurrentRegistrationStatus() {
        const self = this;
        // get period effective status types
        const statusTypes = await this.context.model('PeriodRegistrationEffectiveStatus').getItems();
        const studentStatus = await this.property('studentStatus').getItem();
        if (studentStatus.alternateName !== 'active') {
            return statusTypes.find(function (x) {
                return x.code === 'INACTIVE_STUDENT'
            });
        }
        const online = await self.context.model('Workspace').where('databaseStatus').equal('online').count();
        if (!online) {
            throw new HttpError(503, 'The system is updating. Please try again later.');
        }
        // validate department registration period
        const studentDepartment = await this.property('department').select('id').value();
        // get current date
        const currentDate = moment(new Date()).startOf('day').toDate();
        const department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
            .and('date(registrationPeriodStart)').lowerOrEqual(currentDate)
            .and('date(registrationPeriodEnd)').greaterOrEqual(currentDate)
            .select('id', 'name', 'registrationPeriodStart', 'registrationPeriodEnd', 'currentYear', 'currentPeriod')
            .flatten()
            .silent()
            .getItem();
        // if department is null then return status for closed registration period
        if (department == null) {
            return statusTypes.find(function (x) {
                    return x.code === 'CLOSED_REGISTRATION_PERIOD';
                });
        }
        // check if student can (or should) select specialty
        const canSelectSpecialty = await this.canSelectSpecialty();
        // if result is success
        if (canSelectSpecialty.success === true) {
            // return status for select specialty
            return statusTypes.find(function (x) {
                    return x.code === 'SELECT_SPECIALTY';
                });
        }

        // get current registration
        const registration = await this.context.model('StudentPeriodRegistration')
            .where("student").equal(this.getId()).and("registrationYear")
            .equal(department.currentYear)
            .and("registrationPeriod").equal(department.currentPeriod)
            .expand('classes').getItem();
        // if student does not have a period registration for current academic period
        if (registration == null) {
            // return result for open with no transaction
            return statusTypes.find(function (x) {
                    return x.code === 'OPEN_NO_TRANSACTION'
                });
        }
        //check registration status (closed)
        if (registration.status.alternateName === 'closed') {
            //registration exists and is closed
            return statusTypes.find(function (x) {
                    return x.code === 'CLOSED_REGISTRATION';
                });
        }
        // get last document
        const lastDocument = await this.context.model('StudentRegistrationDocument')
            .where("registration").equal(registration.id)
            .orderByDescending('dateCreated')
            .getItem();
        // if there is no registration document at all
        if (lastDocument == null) {
            // if registration does not
            if (registration.classes.length > 0) {
                //secretary has registered classes
                return statusTypes.find(function (x) {
                    return x.code === 'P_SYSTEM_TRANSACTION';
                });
            }
            return statusTypes.find(function (x) {
                    return x.code === 'OPEN_NO_TRANSACTION'
                });
        }
        // check last document status
        switch (lastDocument.documentStatus.alternateName) {
            case 'pending':
                return statusTypes.find(function (x) {
                        return x.code === 'PENDING_TRANSACTION';
                    });
            case 'closed':
                return statusTypes.find(function (x) {
                        return x.code === 'SUCCEEDED_TRANSACTION'
                    });
            default:
                return statusTypes.find(function (x) {
                    return x.code === 'FAILED_TRANSACTION'
                });
        }
    }


    /**
     * @return {Promise<DataQueryable>}
     */
    @EdmMapping.func("availableSpecialties",EdmType.CollectionOf("StudyProgramSpecialty"))
    getAvailableSpecialties() {
        const self = this;
        return self.getModel().select('id', 'studyProgram').where('id').equal(self.id).flatten().getItem().then(function (result) {
            if (_.isNil(result)) {
                return new HttpNotFoundError();
            }
            return self.context.model('StudyProgramSpecialty').where("studyProgram").equal(result.program).prepare();
        });
    }


    availableProgramSpecialties(callback) {
        const self = this, context = self.context;
        self.programOf(function(err,program) {
            if (err) {
                TraceUtils.error(err);
                return callback(err);
            }
            if (_.isNil(program)) {
                return callback(new Error('Student program is null or undefined'));
            }
            context.model("StudyProgramSpecialty").where("studyProgram").equal(program).and("specialty").notEqual(-1).getAllItems().then(value => {
                return callback(null, value);
            }).catch( err => {
                return callback(err);
            });
        });
    }

    /**
     * @returns {Promise<DataQueryable>}
     */
    @EdmMapping.func('availableClasses',EdmType.CollectionOf('StudentAvailableClass'))
    getAvailableClasses() {
        const self = this;
        return self.getModel().select('id','department').where('id').equal(self.id).expand('department').getItem().then(function(result) {
            if (_.isNil(result)) {
                return Promise.reject(new HttpNotFoundError());
            }
            const entityExpr = util.format('ufnStudentAvailableClasses(%s,%s,%s)',result.department.currentYear, result.department.currentPeriod, result.id);
            const entity = new QueryEntity(entityExpr).as('StudentAvailableClasses');
            const q = self.context.model('StudentAvailableClass').asQueryable();
            //replace entity reference
            q.query.from(entity);
            //return queryable
            return Promise.resolve(q);
        }).catch(function(err) {
            TraceUtils.error(err);
            if (err instanceof HttpError) {
                return Promise.reject(err);
            }
            return Promise.reject(new HttpServerError());
        });
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CanSelectSpecialty:
     *  get:
     *    tags:
     *      - Student
     *    description: Validates if student can select a study program specialty
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func('canSelectSpecialty', ValidationResult)
    async canSelectSpecialty() {
        // check if student is active
        const active = await this.isActive();
        // if student is not active
        if (!active) {
            // throw error
            return new ValidationResult(false, 'ESTATUS', 'Student is not active');
        }
        // get student current registration
        const currentRegistration = await (await this.getCurrentRegistration()).getItem();
        // if current registration is null
        if (currentRegistration == null) {
            // throw error for missing registration
            return new ValidationResult(false, 'EREGISTRATION', 'You must first register to current semester.');
        }
        // get student study program
        const studyProgram = await this.property('studyProgram').expand('specialties').getItem();
        // validate study program
        if (studyProgram == null) {
            return new ValidationResult(false, 'EPROGRAM', 'Study program is null or undefined.');
        }
        if (!Array.isArray(studyProgram.specialties)) {
            return new ValidationResult(false, 'EPROGRAM', 'Study program specialties cannot be validated.');
        }
        if (studyProgram.specialties.length === 0) {
            return new ValidationResult(false, 'EPROGRAM', 'Study program does not have any specialty.');
        }
        // get student current specialty
        const currentSpecialty = await this.getSpecialty();
        if (currentSpecialty && studyProgram.specialties.length === 1) {
            return new ValidationResult(false, 'ESPECIALTY', 'Study program does not have an available specialty other than student current specialty.');
        }
        // check if current specialty is other than base specialty
        if (currentSpecialty && currentSpecialty.specialty > -1) {
            return new ValidationResult(false, 'ESPECIALTY', 'Student has already a study program specialty.');
        }
        // get student department
        const studentDepartment = await this.property('department').select('id').value();
        // get local department and validate registration period
        const currentDate = moment(new Date()).startOf('day').toDate();
        const department = await this.context.model('LocalDepartment').where('id').equal(studentDepartment)
            .and('date(registrationPeriodStart)').lowerOrEqual(currentDate)
            .and('date(registrationPeriodEnd)').greaterOrEqual(currentDate)
            .select('id', 'name', 'registrationPeriodStart','registrationPeriodEnd', 'allowSpecialtySelection').getItem();
        if (department == null) {
            return new ValidationResult(false, 'EPERIOD', 'Invalid student registration period.');
        }
        // validate allow specialty selection
        if (!department.allowSpecialtySelection) {
            return new ValidationResult(false, 'ESPECIALTY', 'Student department does not allow changing specialty.');
        }
        const studentSemester = await this.property('semester').value();
        if (studyProgram.specialtySelectionSemester && studyProgram.specialtySelectionSemester > studentSemester) {
            return new ValidationResult(false, 'ESEMESTER', 'Invalid student semester for selecting or updating student specialty.');
        }
        return new ValidationResult(true, 'SUCC');
    }

    /**
     * @swagger
     *
     * /api/Students/Me/RegisterActions:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns a collection of student register actions
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    @EdmMapping.func('RegisterActions', EdmType.CollectionOf('StudentRegisterAction'))
    /**
     * @return DataQueryable
     */
    getRegisterActions() {
        return this.context.model('StudentRegisterAction').where('object').equal(this.getId()).prepare();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CurrentRegisterAction:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns an object which represents a register action for current academic period
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/StudentRegisterAction'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */

    /**
     * @returns {Promise<StudentRegisterAction>}
     */
    @EdmMapping.func('CurrentRegisterAction', 'StudentRegisterAction')
    async getCurrentRegisterAction() {
        // noinspection JSCheckFunctionSignatures
        let q = await this.context.model('StudentRegisterAction')
            .filter('registrationYear eq $it/object/department/currentYear and registrationPeriod eq $it/object/department/currentPeriod');
        return q.and('object').equal(this.getId()).prepare();
    }

    /**
     * @swagger
     *
     * /api/Students/Me/CurrentRegisterAction:
     *  post:
     *    tags:
     *      - Student
     *    description: Save student register action for current academic period
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/StudentRegisterAction'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * @returns {Promise<StudentRegisterAction|*>}
     */
    @EdmMapping.action('CurrentRegisterAction', 'StudentRegisterAction')
    async saveCurrentRegisterAction() {
        // get silent mode from parent
        const silent = this.getModel().isSilent();
        // get current academic year and period
        const student = await this.context.model('Student')
            .where('id').equal(this.getId())
            .select('id', 'department/currentYear as currentYear', 'department/currentPeriod as currentPeriod')
            .silent()
            .getItem();
        // set object equal to this
        const registerAction ={
            object: student.id,
            registrationYear: student.currentYear,
            registrationPeriod: student.currentPeriod
        };
        // save register action
        await this.context.model('StudentRegisterAction').silent(silent).save(registerAction);
        // return original register action
        return await this.context.model('StudentRegisterAction').where('id').equal(registerAction.id).silent(silent).getItem();
    }

    async isActive() {
        return await this.getModel().where('id').equal(this.getId())
            .and('studentStatus/alternateName').equal('active').count();
    }

    /**
     * @swagger
     * /api/Students/Me/Specialty:
     *  get:
     *    tags:
     *      - Student
     *    description: Returns student's current study program specialty
     *    security:
     *      - OAuth2:
     *          - students
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *            schema:
     *              $ref: '#/components/schemas/StudyProgramSpecialty'
     *      '403':
     *        description: forbidden
     *      '404':
     *        description: not found
     *      '500':
     *        description: internal server error
     */
    /**
     * Gets study program specialty
     * @returns {Promise<StudyProgramSpecialty>}
     */
    @EdmMapping.func('specialty', 'StudyProgramSpecialty')
    async getSpecialty() {
        // get student study program and specialty
        return this.property('studyProgramSpecialty').getItem();
    }

    /**
     * @swagger
     *
     *  /api/Students/Me/Specialty:
     *   post:
     *    tags:
     *      - Student
     *    description: Initializes an action for selecting a study program specialty by student. This action may be automatically completed.
     *    security:
     *     - OAuth2:
     *        - students
     *    requestBody:
     *      content:
     *        application/json:
     *          schema:
     *            $ref: '#/components/schemas/StudyProgramSpecialty'
     *    responses:
     *      '200':
     *        description: success
     *        content:
     *          application/json:
     *              schema:
     *                  $ref: '#/components/schemas/UpdateSpecialtyAction'
     *      '404':
     *        description: not found
     *      '409':
     *        description: conflict
     *      '500':
     *        description: internal server error
     */
    /**
     * Gets study program specialty
     * @returns {Promise<StudyProgramSpecialty>}
     */
    @EdmMapping.param('specialty', 'StudyProgramSpecialty', false, true)
    @EdmMapping.action('specialty', 'UpdateSpecialtyAction')
    async updateSpecialty(specialty) {
            if (specialty == null) {
                throw new HttpBadRequestError('Expected a study program specialty');
            }
            // create a new update specialty action
            const action = {
                object: this,
                specialty: specialty
            };
            // select specialty validation
            const validationResult = await this.canSelectSpecialty();
            // if validation has been failed
            if (validationResult.success === false) {
                // send conflict error
                throw Object.assign(new HttpConflictError(validationResult.message), {
                    code: validationResult.code
                })
            }
            // check if an active action already exists
            let exists = await this.context.model('UpdateSpecialtyAction')
                .where('object').equal(this.id).and('actionStatus/alternateName').equal(ActionStatusType.ActiveActionStatus)
                .count();
            // and throw conflict error
            if (exists) {
                throw Object.assign(new HttpConflictError('An active update specialty action already exists.'), {
                    code: 'ECONSTRAINT'
                });
            }
            // save update specialty action
            await this.context.model('UpdateSpecialtyAction').save(action);
            // return result
            return await this.context.model('UpdateSpecialtyAction').where('id').equal(action.id).getItem();
    }


}

module.exports = Student;
