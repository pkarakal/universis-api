import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import StudyProgram = require('./study-program-model');

/**
 * @class
 */
declare class StudyProgramSpecialty extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός της κατεύθυνσης.
      */
     public id: string; 
     
     /**
      * @description Το πρόγραμμα σπουδών στο οποίο ανήκει η κατεύθυνση
      */
     public studyProgram: StudyProgram|any; 
     
     /**
      * @description Ο κωδικός της κατεύθυνσης
      */
     public specialty: number; 
     
     /**
      * @description Μία σύντομη περιγραφή για την κατεύθυνση
      */
     public name: string; 
     
     /**
      * @description Η συντομογραφία της κατεύθυνσης
      */
     public abbreviation?: string; 

}

export = StudyProgramSpecialty;