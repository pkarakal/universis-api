import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import Thesis = require('./thesis-model');
import Student = require('./student-model');

/**
 * @class
 */
declare class StudentThesis extends DataObject {

     
     /**
      * @description Ο μοναδικός κωδικός του αντικειμένου
      */
     public id: number; 
     
     /**
      * @description Η εργασία
      */
     public thesis: Thesis|any; 
     
     /**
      * @description Ο φοιτητής
      */
     public student: Student|any; 
     
     /**
      * @description Η ημερομηνία ολοκλήρωσης της εργασίας
      */
     public dateCompleted?: Date; 
     
     /**
      * @description Βαθμός (0-1)
      */
     public grade?: number; 
     
     /**
      * @description Ο βαθμός της εργασίας έχει περαστεί.
      */
     public isPassed?: number; 
     
     /**
      * @description Βαθμός (ανηγμένος στην κλίμακα βαθμολογίας της εργασίας).
      */
     public formattedGrade?: string; 
     
     /**
      * @description Ημερομηνία τροποποίησης
      */
     public dateModified: Date; 

}

export = StudentThesis;