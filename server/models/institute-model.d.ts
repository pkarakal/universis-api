import {DataObject} from '@themost/data/data-object';
import InstituteConfiguration = require('./institute-configuration-model');
/**
 * @class
 */
declare class Institute extends DataObject {

     
     /**
      * @description The identifier of the item.
      */
     public id?: number; 
     
     /**
      * @description The name of the item.
      */
     public name: string; 
     
     /**
      * @description An alias for the item.
      */
     public alternateName?: string; 
     
     /**
      * @description The identifier property represents any kind of identifier for any kind of object.
      */
     public identifier?: string; 
     
     /**
      * @description URL of the item.
      */
     public url?: string; 
     
     /**
      * @description A boolean which indicates whether this institute is the local organization or not.
      */
     public local?: boolean;
     /**
      * @description An object which represents current institute configuration.
      */
     public instituteConfiguration?: InstituteConfiguration;
     /**
      * @description Returns a collection of institute registration period configuration
      */
     public registrationPeriods: Array<any>

}

export = Institute;
