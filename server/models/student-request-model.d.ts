import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import User = require('./user-model');
import Student = require('./student-model');

/**
 * @class
 */
declare class StudentRequest extends DataObject {

     
     /**
      * @description Μοναδικός κωδικός εγγραφής
      */
     public id: number; 
     
     /**
      * @description Κωδικός χρήστη
      */
     public userId: User|any; 
     
     /**
      * @description Κωδικός φοιτητή
      */
     public student?: Student|any; 
     
     /**
      * @description Κωδικός τύπου αίτησης
      */
     public requestId: string; 
     
     /**
      * @description Ονομασία
      */
     public name: string; 
     
     /**
      * @description Περιγραφή
      */
     public description: string; 
     
     /**
      * @description Ημερομηνία αίτησης
      */
     public dateRequested: Date; 
     
     /**
      * @description Status Id
      */
     public statusId?: number; 
     
     /**
      * @description Status
      */
     public status?: string; 
     
     /**
      * @description Αρ. αντιγράφων
      */
     public numberOfCopies: number; 
     
     /**
      * @description Παραλαβή από
      */
     public receiveFrom?: string; 
     
     /**
      * @description Αποστολή στην ηλεκτρονική διεύθυνση
      */
     public sentToEmailAddress?: string; 
     
     /**
      * @description Αποστολή στη διεύθυνση
      */
     public sentToAddress?: string; 
     
     /**
      * @description Σχετικό αρχείο
      */
     public relativeFile?: string; 
     
     /**
      * @description Date Modified
      */
     public dateModified: Date; 

}

export = StudentRequest;