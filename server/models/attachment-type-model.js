import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} alternateName
 * @property {number} identifier
 * @property {string} additionalType
 * @property {string} description
 * @property {string} image
 * @property {string} name
 * @property {string} url
 * @property {Date} dateCreated
 * @property {Date} dateModified
 * @property {User|any} createdBy
 * @property {User|any} modifiedBy
 * @augments {DataObject}
 */
@EdmMapping.entityType('AttachmentType')
class AttachmentType extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = AttachmentType;