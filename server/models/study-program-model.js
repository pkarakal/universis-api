import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {Department|any} department
 * @property {StudyLevel|any} studyLevel
 * @property {boolean} isActive
 * @property {string} name
 * @property {string} abbreviation
 * @property {string} printName
 * @property {string} degreeDescription
 * @property {GradeScale|any} gradeScale
 * @property {number} decimalDigits
 * @property {boolean} hasFees
 * @property {number} semesters
 * @property {Semester|any} specialtySelectionSemester
 * @property {Array<StudyProgramSpecialty|any>} specialties
 */
@EdmMapping.entityType('StudyProgram')
class StudyProgram extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = StudyProgram;
