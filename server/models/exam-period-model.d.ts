import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';
/**
 * @class
 */
declare class ExamPeriod extends DataObject {

     
     /**
      * @description Id
      */
     public id: number; 
     
     /**
      * @description Η ονομασία της εξεταστικής.
      */
     public name: string; 

}

export = ExamPeriod;