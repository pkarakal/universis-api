import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import CourseExam = require('./course-exam-model');
import Instructor = require('./instructor-model');

/**
 * @class
 */
declare class CourseExamInstructor extends DataObject {

     
     /**
      * @description Ο κωδικός σύνδεσης εξεταστή - εξέτασης
      */
     public id: string; 
     
     /**
      * @description Η εξέταση του μαθήματος
      */
     public courseExam: CourseExam|any; 
     
     /**
      * @description Ο εξεταστής του μαθήματος για τη συγκεκριμένη εξέταση
      */
     public instructor: Instructor|any; 
     
     /**
      * @description Η ημερομηνία τροποποίησης.
      */
     public dateModified: Date; 

}

export = CourseExamInstructor;