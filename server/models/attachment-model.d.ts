import {EdmMapping,EdmType} from '@themost/data/odata';

import {DataObject} from '@themost/data/data-object';import AttachmentType = require('./attachment-type-model');
import User = require('./user-model');

/**
 * @class
 */
declare class Attachment extends DataObject {

     
     public id: number; 
     
     public additionalType?: string; 
     
     /**
      * @description An alias for the item.
      */
     public alternateName: string; 
     
     /**
      * @description A content type for this item.
      */
     public contentType: string; 
     
     /**
      * @description Date of first broadcast/publication.
      */
     public datePublished?: Date; 
     
     /**
      * @description Indicates whether the item is published or not.
      */
     public published: boolean; 
     
     /**
      * @description The keywords/tags used to describe this content.
      */
     public keywords?: string; 
     
     /**
      * @description A thumbnail image relevant to the item.
      */
     public thumbnail?: string; 
     
     /**
      * @description The version of this item.
      */
     public version?: string; 
     
     /**
      * @description The attachment type
      */
     public attachmentType?: AttachmentType|any; 
     
     /**
      * @description Μία σύντομη περιγραφή για το στοιχείο.
      */
     public description?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση URL της εικόνας του στοιχείου.
      */
     public image?: string; 
     
     /**
      * @description Το όνομα του στοιχείου.
      */
     public name?: string; 
     
     /**
      * @description Η ηλεκτρονική διεύθυνση με περισσότερες πληροφορίες για το στοιχείο. Π.χ. τη διεύθυνση URL της σελίδας Wikipedia ή του επίσημου ιστότοπου.
      */
     public url?: string; 
     
     /**
      * @description Η ημερομηνία δημιουργίας του στοιχείου
      */
     public dateCreated?: Date; 
     
     /**
      * @description Η ημερομηνία τροποποίησης του στοιχείου
      */
     public dateModified?: Date; 
     
     /**
      * @description Ο χρήστης που δημιούργησε το στοιχείο.
      */
     public createdBy?: User|any; 
     
     /**
      * @description Ο χρήστης που τροποποίησε το στοιχείο.
      */
     public modifiedBy?: User|any; 

}

export = Attachment;