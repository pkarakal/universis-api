import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
let Action = require('./action-model');
/**
 * @class
 * @property {number} id
 */
@EdmMapping.entityType('UpdateAction')
class UpdateAction extends Action {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = UpdateAction;
